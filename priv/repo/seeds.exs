# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CheetahApi.Repo.insert!(%CheetahApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias CheetahApi.Repo
alias CheetahApi.Account.Role

Repo.insert_all(Role, [[name: "super_admin"],
                       [name: "site_admin"],
                       [name: "student"],
                       [name: "faculty"],
                       [name: "alumni"]])
