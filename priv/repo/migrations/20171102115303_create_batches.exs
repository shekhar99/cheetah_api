defmodule CheetahApi.Repo.Migrations.CreateBatches do
  use Ecto.Migration

  def change do
    create table(:batches) do
      add :start_date, :date
      add :end_date, :date

      add :stream_id, references(:streams, on_delete: :delete_all)
      timestamps()
    end

  end
end
