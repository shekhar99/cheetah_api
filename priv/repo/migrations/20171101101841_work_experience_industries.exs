defmodule CheetahApi.Repo.Migrations.WorkExperienceIndustries do
  use Ecto.Migration

  def change do
    create table(:work_experience_industries) do
      add :work_experience_id, references(:work_experiences, on_delete: :delete_all)
      add :industry_id, references(:industries)
    end

    create index(:work_experience_industries, [:work_experience_id])
    create index(:work_experience_industries, [:industry_id])
    create unique_index(:work_experience_industries, [:work_experience_id, :industry_id])
  end
end
