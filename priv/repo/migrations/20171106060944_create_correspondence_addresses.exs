defmodule CheetahApi.Repo.Migrations.CreateCorrespondenceAddresses do
  use Ecto.Migration

  def change do
    create table(:correspondence_addresses) do
      add :address, :string
      add :locality, :string
      add :city, :string
      add :postal_code, :string

      add :contact_detail_id, references(:contact_details, on_delete: :delete_all)

      timestamps()      
    end

    create index(:correspondence_addresses, [:contact_detail_id])
  end
end
