defmodule CheetahApi.Repo.Migrations.CreateProfessionalRoles do
  use Ecto.Migration

  def change do
    create table(:professional_roles) do
      add :name, :string

      timestamps()
    end

    create unique_index(:professional_roles, [:name])
  end
end
