defmodule CheetahApi.Repo.Migrations.CreateCourses do
  use Ecto.Migration

  def change do
    create table(:courses) do
      add :name, :string
      #Duration is in months
      add :duration, :integer
    end
  end
end
