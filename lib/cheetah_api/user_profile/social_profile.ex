defmodule CheetahApi.UserProfile.SocialProfile do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.User

  schema "social_profiles" do
    field :name
    field :url

    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(name url)a

  def changeset(struct, params\\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end

end