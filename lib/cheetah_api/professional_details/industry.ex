defmodule CheetahApi.ProfessionalDetails.Industry do
  use CheetahApi.Data

  schema "industries" do
    field :name, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:name])
      |> validate_required([:name])
  end

end