defmodule CheetahApi.ProfessionalDetails.WorkExperience do
  use CheetahApi.Data
  
  alias CheetahApi.ProfessionalDetails.{ProfessionalRole, Industry, Skill, WorkDetail}
  alias CheetahApi.UserProfile.User

  schema "work_experiences" do
    field :professional_headline, :string
    field :total_years_experience, :integer

    has_many :work_details, WorkDetail

    many_to_many :professional_roles, ProfessionalRole, join_through: "work_experience_professional_roles", on_replace: :delete
    many_to_many :industries, Industry, join_through: "work_experience_industries", on_replace: :delete
    many_to_many :skills, Skill, join_through: "work_experience_skills", on_replace: :delete

    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(total_years_experience user_id)a
  @optional_fields ~w(professional_headline)a

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, @required_fields ++ @optional_fields)
      |> validate_required(@required_fields)
      |> put_assoc(:professional_roles, load_assoc(params["professional_role_ids"], ProfessionalRole))
      |> put_assoc(:industries, load_assoc(params["industry_ids"], Industry))
      |> put_assoc(:skills, load_assoc(params["skill_ids"], Skill))
      |> cast_assoc(:work_details)
  end

  defp load_assoc(nil, schema), do: load_assoc([], schema)
  defp load_assoc(ids, schema), do: Repo.all(from s in schema, where: s.id in ^ids)

end