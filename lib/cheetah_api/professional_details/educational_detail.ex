defmodule CheetahApi.ProfessionalDetails.EducationalDetail do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.User

  schema "educational_details" do
    field :institution, :string
    field :degree, :string
    field :start_date, :date
    field :end_date, :date
    field :location, :string

    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(institution degree start_date location)a
  @optional_fields ~w(end_date)a

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, @required_fields ++ @optional_fields)
      |> validate_required(@required_fields)
  end

end
