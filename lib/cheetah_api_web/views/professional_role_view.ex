defmodule CheetahApiWeb.ProfessionalRoleView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :name
  ]
end