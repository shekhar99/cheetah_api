defmodule CheetahApiWeb.WorkDetailView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :description, :start_date, :end_date, :location, :work_experience_id, :professional_role_id, :company_id
  ]

end
