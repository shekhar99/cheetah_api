defmodule CheetahApiWeb.Router do
  use CheetahApiWeb, :router

  pipeline :api do
    plug :accepts, ["json-api", "json"]
    plug JaSerializer.Deserializer
  end

  scope "/api", CheetahApiWeb do
    pipe_through :api

    post "/register_email", AccountController, :register_email
    post "/create_account", AccountController, :create_account

    resources "/users", UserController, only: [:show, :update]
    resources "/contact_detail", ContactDetailController, only: [:show, :update]
    resources "/work_experience", WorkExperienceController, only: [:show, :update]
    resources "/educational_detail", EducationalDetailController
    resources "/work_detail", WorkDetailController
  end
end
