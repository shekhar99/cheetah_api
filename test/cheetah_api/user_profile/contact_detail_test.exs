defmodule CheetahApi.UserProfile.ContactDetailTest do
  use CheetahApi.DataCase

  alias CheetahApi.UserProfile.ContactDetail

  @valid_attrs %{
    primary_email: "shekhar@gmail.com", current_city: "pune", mobile_number: "0123456789"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ContactDetail.changeset(%ContactDetail{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ContactDetail.changeset(%ContactDetail{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "website url must be in proper format" do
    attrs = Map.put(@valid_attrs, :website_url, "website")
    changeset = ContactDetail.changeset(%ContactDetail{}, attrs)
    assert %{website_url: ["has invalid format"]} = errors_on(changeset)
  end

end