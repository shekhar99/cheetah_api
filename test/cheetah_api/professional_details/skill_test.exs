defmodule CheetahApi.ProfessionalDetails.SkillTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails.Skill

  @valid_attrs %{
    name: "Elixir/Phoenix"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Skill.changeset(%Skill{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Skill.changeset(%Skill{}, @invalid_attrs)
    refute changeset.valid?
  end

end