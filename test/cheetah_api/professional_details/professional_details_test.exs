defmodule CheetahApi.ProfessionalDetailsTest do
  use CheetahApi.DataCase

  alias CheetahApi.ProfessionalDetails
  alias CheetahApi.ProfessionalDetails.{WorkExperience, WorkDetail, EducationalDetail}

  import CheetahApi.Factory

  describe "work experience" do
    @create_attrs %{
      total_years_experience: 5, professional_headline: "this is test headline"
    }
    @update_attrs %{
      total_years_experience: 7, professional_headline: "this is updated headline"
    }
    @invalid_attrs %{}

    test "create and return work experience if valid params are passed" do
      user = insert(:user)
      attrs = Map.put(@create_attrs, :user_id, user.id)
      assert {:ok, %WorkExperience{} = work_experience} = ProfessionalDetails.create_work_experience(attrs)
      assert work_experience.total_years_experience == 5
      assert work_experience.professional_headline == "this is test headline"
    end

    test "create and return error if invalid params are passed" do
      assert {:error, %Ecto.Changeset{}} = ProfessionalDetails.create_work_experience(@invalid_attrs)
    end

    test "return work experience if found" do
      work_experience = insert(:work_experience)
      assert (%WorkExperience{} = we) = ProfessionalDetails.get_work_experience(%{"id" => work_experience.id})
      assert we.id == work_experience.id
    end

    test "return nil if work experience not found" do
      refute ProfessionalDetails.get_work_experience(%{"id" => -1})
    end

    # test "update and return work experience" do
    #   work_experience = insert(:work_experience)
    #   assert {:ok, %WorkExperience{} = work_experience} = ProfessionalDetails.update_work_experience(work_experience, @update_attrs)
    # end

  end

  describe "work detail" do
    @create_attrs %{
      description: "test description", start_date: "2016-04-25",
      end_date: "2018-04-25", location: "pune"
    }
    @update_attrs %{
      description: "updated description", location: "jhansi"
    }
    @invalid_attrs %{}

    test "return work detail if found" do
      work_detail = insert(:work_detail)
      assert (%WorkDetail{} = wd) = ProfessionalDetails.get_work_detail(%{"id" => work_detail.id})
      assert wd.id == work_detail.id
    end

    test "return nil if work detail not found" do
      refute ProfessionalDetails.get_work_detail(%{"id" => -1})
    end

    test "create and return work detail if valid params are passed" do
      work_experience_id = insert(:work_experience).id
      professional_role_id = insert(:professional_role).id
      company_id = insert(:company).id

      attrs = Map.merge(
        @create_attrs, 
        %{work_experience_id: work_experience_id, professional_role_id: professional_role_id, company_id: company_id}
      )

      assert {:ok, %WorkDetail{} = work_detail} = ProfessionalDetails.create_work_detail(attrs)
      assert work_detail.description == "test description"
      assert work_detail.start_date == ~D[2016-04-25]
      assert work_detail.end_date == ~D[2018-04-25]
      assert work_detail.location == "pune"
      assert work_detail.work_experience_id == work_experience_id
      assert work_detail.professional_role_id == professional_role_id
      assert work_detail.company_id == company_id

    end

    test "create and return error if invalid params are passed" do
      assert {:error, %Ecto.Changeset{}} = ProfessionalDetails.create_work_detail(@invalid_attrs)
    end

    test "update work detail if valid attrs are passed" do
      work_detail = insert(:work_detail)
      assert {:ok, %WorkDetail{} = work_detail} = ProfessionalDetails.update_work_detail(work_detail, @update_attrs)
      assert work_detail.description == "updated description"
      assert work_detail.location == "jhansi"
    end

    test "delete work detail" do
      work_detail = insert(:work_detail)
      assert {:ok, %WorkDetail{}} = ProfessionalDetails.delete_work_detail(work_detail)
    end
  end

  describe "educational detail" do
    @create_attrs %{
      institution: "ait pune", degree: "BE", start_date: "2016-04-25", location: "pune"
    }
    @update_attrs %{
      degree: "ME", location: "jhansi"
    }
    @invalid_attrs %{}

    test "create and return educational detail if valid params are passe" do
      user = insert(:user)
      attrs = Map.put(@create_attrs, :user_id, user.id)
      assert {:ok, %EducationalDetail{} = educational_detail} = ProfessionalDetails.create_educational_detail(attrs)
      assert educational_detail.institution == "ait pune"
      assert educational_detail.degree == "BE"
      assert educational_detail.start_date == ~D[2016-04-25]
      assert educational_detail.location == "pune"
    end

    test "create and return changeset error if invalid params are passed" do
      assert {:error, %Ecto.Changeset{}} = ProfessionalDetails.create_educational_detail(@invalid_attrs)
    end

    test "get educational detail if found" do
      educational_detail = insert(:educational_detail)
      assert (%EducationalDetail{} = ed) = ProfessionalDetails.get_educational_detail(%{"id" => educational_detail.id})
      assert ed.id == educational_detail.id
    end

    test "return nil if educational detail if not found" do
      refute ProfessionalDetails.get_educational_detail(%{"id" => -1})
    end

    test "update and return educational detail" do
      educational_detail = insert(:educational_detail)
      assert {:ok, %EducationalDetail{} = ed} = ProfessionalDetails.update_educational_detail(educational_detail, @update_attrs)
      assert ed.degree == "ME"
      assert ed.location == "jhansi"
    end

    test "delete educational_detail" do
      educational_detail = insert(:educational_detail)
      assert {:ok, %EducationalDetail{}} = ProfessionalDetails.delete_educational_detail(educational_detail)
    end

  end
end