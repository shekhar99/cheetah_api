defmodule CheetahApiWeb.UserControllerTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.UserProfile.User

  @create_attrs %{
    first_name: "chandra", last_name: "shekhar",
    email_address: "shekhar@gmail.com", gender: "male",
    dob: "1993-04-25"
  }

  @update_attrs %{
    first_name: "shekhar", last_name: "chandra",
    email_address: "chandra@gmail.com"
  }

  @invalid_attrs %{
    email_address: "shekhargmail.com"
  }

  describe "show/2" do
    setup [:create_user]
    test "responds with user info if the user is found", %{conn: conn, user: user} do
      response = 
        conn
        |> get(user_path(conn, :show, user.id))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
              "dob" => dob, "emailAddress" => email,
              "gender" => gender, "firstName" => first_name, "lastName" => last_name
            }}} <- response do
        assert first_name == "chandra"
        assert last_name == "shekhar"
        assert dob == "1993-04-25"
        assert email == "shekhar@gmail.com"
        assert gender == "male"
      else
        _ -> assert false
      end
    end

    test "responds with a message indicating user not found", %{conn: conn} do
      response = 
        conn
        |> get(user_path(conn, :show, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false        
      end
    end

  end

  describe "update/2" do
    setup [:create_user]
    test "Edits, and responds with the user if attributes are valid", %{conn: conn, user: user} do
      response = 
        conn
        |> put(user_path(conn, :update, user.id, @update_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
              "emailAddress" => email, "firstName" => first_name, "lastName" => last_name
            }}} <- response do
        assert first_name == "shekhar"
        assert last_name == "chandra"
        assert email == "chandra@gmail.com"
      else
        _ -> assert false        
      end
    end

    test "Returns an error and does not edit the user if attributes are invalid", %{conn: conn, user: user} do
      response = 
        conn
        |> put(user_path(conn, :update, user.id, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"email_address" => email_error}} <- response do
        assert email_error == "has invalid format"
      else
        _ -> assert false        
      end
    end

  end

  defp create_user(_) do
    {:ok, user} =
      %User{}
      |> User.changeset(@create_attrs)
      |> CheetahApi.Repo.insert

    {:ok, user: user}
  end

end