defmodule CheetahApiWeb.ContactDetailControllerTest do
  use CheetahApiWeb.ConnCase

  alias CheetahApi.UserProfile.ContactDetail

  @create_attrs %{
    primary_email: "chandra@gmail.com", current_city: "pune", mobile_number: "1234567890"
  }

  @update_attrs %{
    secondary_email: "shekhar@gmail.com", website_url: "www.website.com", home_town: "jhansi"
  }

  @invalid_attrs %{
    secondary_email: "shekhargmail.com", website_url: "website"
  }

  describe "show/2" do
    setup [:create_contact_detail]
    test "responds with contact_detail info if contact_detail is found", %{conn: conn, contact_detail: contact_detail} do
      response = 
        conn
        |> get(contact_detail_path(conn, :show, contact_detail.id))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "primaryEmail" => email, "currentCity" => city, "mobileNumber" => mobile
      }}} <- response do
        assert email == "chandra@gmail.com"
        assert city == "pune"
        assert mobile == "1234567890"
      else
        _ -> assert false
      end
    end

    test "responds with a message indicating contact_detail not found", %{conn: conn} do
      response = 
        conn
        |> get(contact_detail_path(conn, :show, -1))
        |> json_response(404)

      with %{"errors" => %{"detail" => detail, "status" => status}} <- response do
        assert detail == "Not found"
        assert status == 404
      else
        _ -> assert false
      end
    end
  end

  describe "update/2" do
    setup [:create_contact_detail]
    test "updates contact_detail info if valid attributes are passed", %{conn: conn, contact_detail: contact_detail} do
      response = 
        conn
        |> put(contact_detail_path(conn, :update, contact_detail.id, @update_attrs))
        |> json_response(200)

      with %{"data" => %{"attributes" => %{
        "secondaryEmail" => email, "websiteUrl" => website, "homeTown" => town
      }}} <- response do
        assert email == "shekhar@gmail.com"
        assert town == "jhansi"
        assert website == "www.website.com"
      else
        _ -> assert false
      end
    end

    test "returns an error and will not update contact_detail if the invalid attrs are passed", %{conn: conn, contact_detail: contact_detail} do
      response = 
        conn
        |> put(contact_detail_path(conn, :update, contact_detail.id, @invalid_attrs))
        |> json_response(422)

      with %{"errors" => %{"secondary_email" => invalid_format, "website_url" => invalid_format}} <- response 
      do
        assert invalid_format == "has invalid format"
      else
        _ -> assert false
      end
    end
  end

  defp create_contact_detail(_) do
    {:ok, contact_detail} =
      %ContactDetail{}
      |> ContactDetail.changeset(@create_attrs)
      |> CheetahApi.Repo.insert

    {:ok, contact_detail: contact_detail}
  end

end